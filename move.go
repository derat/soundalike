// Copyright 2024 Daniel Erat.
// All rights reserved.

package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
)

// moveInteractive prompts the user to keep one file from infos
// and moves the other files from srcDir to dstDir.
func moveInteractive(stdout io.Writer, stdin io.Reader,
	infos []*fileInfo, srcDir, dstDir string) error {
	sort.Slice(infos, func(i, j int) bool { return infos[i].size > infos[j].size })
	width := strconv.Itoa(len(strconv.Itoa(len(infos))))
	for i, ln := range formatFiles(infos, "") {
		fmt.Fprintf(stdout, "%"+width+"d. %v\n", i+1, ln)
	}
	var keep int // 0-indexed
	r := bufio.NewReader(stdin)
	for {
		fmt.Fprintf(stdout, "File to keep [%d-%d, empty to keep all]: ", 1, len(infos))
		input, err := r.ReadString('\n')
		if err == io.EOF {
			fmt.Fprintln(stdout)
			return nil
		} else if err != nil {
			return err
		}
		if input = strings.TrimSpace(input); input == "" {
			return nil
		} else if v, err := strconv.Atoi(input); err != nil {
			fmt.Fprintln(stdout, "Invalid input")
		} else if v < 1 || v > len(infos) {
			fmt.Fprintf(stdout, "Selection not in range 1-%d\n", len(infos))
		} else {
			keep = v - 1
			break
		}
	}
	for i, info := range infos {
		if i == keep {
			continue
		}
		srcPath := filepath.Join(srcDir, info.path)
		dstPath := filepath.Join(dstDir, info.path)
		fmt.Fprintf(stdout, "Moving %v -> %v\n", srcPath, dstPath)
		if err := moveFile(srcPath, dstPath); err != nil {
			return err
		}
	}
	return nil
}

// moveSmaller keeps the largest file from infos and moves the other files from srcDir to dstDir.
func moveSmaller(stdout io.Writer, infos []*fileInfo, srcDir, dstDir string) error {
	sort.Slice(infos, func(i, j int) bool { return infos[i].size > infos[j].size })
	for _, ln := range formatFiles(infos, "") {
		fmt.Fprintln(stdout, ln)
	}
	for _, info := range infos[1:] {
		srcPath := filepath.Join(srcDir, info.path)
		dstPath := filepath.Join(dstDir, info.path)
		fmt.Fprintf(stdout, "Moving %v -> %v\n", srcPath, dstPath)
		if err := moveFile(srcPath, dstPath); err != nil {
			return err
		}
	}
	return nil
}

// moveFile renames srcPath to dstPath, creating the destination directory if needed.
func moveFile(srcPath, dstPath string) error {
	if err := prepareMove(srcPath, dstPath); err != nil {
		return err
	}
	if err := os.Rename(srcPath, dstPath); err == nil {
		return nil
	}
	// If the rename failed, maybe the paths were on different partitions.
	// Try copying and unlinking instead.
	return copyAndRemoveFile(srcPath, dstPath)
}

// copyAndRemoveFile copies srcPath to dstPath and unlinks srcPath.
// This function is separate from moveFile so it can be exercised by tests.
func copyAndRemoveFile(srcPath, dstPath string) error {
	// This is redundant when called by moveFile, but it doesn't hurt to be safe.
	if err := prepareMove(srcPath, dstPath); err != nil {
		return err
	}
	sf, err := os.Open(srcPath)
	if err != nil {
		return err
	}
	defer sf.Close()

	df, err := os.Create(dstPath)
	if err != nil {
		return err
	}
	if _, err := io.Copy(df, sf); err != nil {
		df.Close()
		return err
	}
	if err := df.Close(); err != nil {
		return err
	}
	return os.Remove(srcPath)
}

// prepareMove checks that srcPath and dstPath are different,
// that srcPath exists and dstPath doesn't, and creates dstPath's
// parent directory if it doesn't already exist.
func prepareMove(srcPath, dstPath string) error {
	srcAbs, err := filepath.Abs(srcPath)
	if err != nil {
		return err
	}
	dstAbs, err := filepath.Abs(dstPath)
	if err != nil {
		return err
	}
	// This is probably redundant since we check that dstPath doesn't exist later.
	if srcAbs == dstAbs {
		return errors.New("can't move file to itself")
	}
	if _, err := os.Stat(srcPath); err != nil {
		return err
	}
	if _, err := os.Stat(dstPath); err == nil {
		return fmt.Errorf("%v already exists", dstPath)
	}
	return os.MkdirAll(filepath.Dir(dstPath), 0755)
}
