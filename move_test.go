// Copyright 2024 Daniel Erat.
// All rights reserved.

package main

import (
	"bytes"
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"sort"
	"strings"
	"testing"
)

func TestMoveSmaller(t *testing.T) {
	medium := fileInfo{id: 1, path: "medium.txt", size: 12, duration: 43.0}
	big := fileInfo{id: 2, path: "subdir/big.txt", size: 24, duration: 42.0}
	small := fileInfo{id: 3, path: "small.txt", size: 6, duration: 46.0}
	infos := []*fileInfo{&medium, &big, &small}

	td := t.TempDir()
	srcDir := filepath.Join(td, "src")
	writeTestFileInfos(t, srcDir, infos)
	dstDir := filepath.Join(td, "dst")
	var stdout bytes.Buffer
	if err := moveSmaller(&stdout, infos, srcDir, dstDir); err != nil {
		t.Fatalf("moveSmaller(stdout, %+v, %q, %q) failed: %v", infos, srcDir, dstDir, err)
	}
	checkTestDir(t, srcDir, map[string]string{big.path: makeTestData(big.size)})
	checkTestDir(t, dstDir, map[string]string{
		small.path:  makeTestData(small.size),
		medium.path: makeTestData(medium.size),
	})
}

func TestMoveInteractive(t *testing.T) {
	medium := fileInfo{id: 1, path: "medium.txt", size: 12, duration: 43.0}
	big := fileInfo{id: 2, path: "subdir/big.txt", size: 24, duration: 42.0}
	small := fileInfo{id: 3, path: "small.txt", size: 6, duration: 46.0}
	infos := []*fileInfo{&medium, &big, &small}

	td := t.TempDir()
	srcDir := filepath.Join(td, "src")
	writeTestFileInfos(t, srcDir, infos)
	dstDir := filepath.Join(td, "dst")

	// Without any input, nothing should be moved.
	var stdout, stdin bytes.Buffer
	if err := moveInteractive(&stdout, &stdin, infos, srcDir, dstDir); err != nil {
		t.Fatalf("moveInteractive(stdout, stdin, %+v, %q, %q) failed: %v", infos, srcDir, dstDir, err)
	}
	checkTestDir(t, srcDir, map[string]string{
		big.path:    makeTestData(big.size),
		small.path:  makeTestData(small.size),
		medium.path: makeTestData(medium.size),
	})
	checkTestDir(t, dstDir, nil)

	// Run again with some bogus input before selecting the second file
	// (which should be medium after sorting by descending size).
	stdout.Reset()
	stdin.WriteString("-1\n0\nabc\n4\n2\n")
	if err := moveInteractive(&stdout, &stdin, infos, srcDir, dstDir); err != nil {
		t.Fatalf("moveInteractive(stdout, stdin, %+v, %q, %q) failed: %v", infos, srcDir, dstDir, err)
	}
	checkTestDir(t, srcDir, map[string]string{medium.path: makeTestData(medium.size)})
	checkTestDir(t, dstDir, map[string]string{
		small.path: makeTestData(small.size),
		big.path:   makeTestData(big.size),
	})
}

func TestMoveFile(t *testing.T) {
	type errorState int
	const (
		noError errorState = iota
		dstExists
		dstIsSrc
		dstIsSrcWithDot
	)

	for _, tc := range []struct {
		name       string
		fn         func(string, string) error
		errorState errorState
	}{
		{"moveFile", moveFile, noError},
		{"moveFile", moveFile, dstExists},
		{"moveFile", moveFile, dstIsSrc},
		{"copyAndRemoveFile", copyAndRemoveFile, noError},
		{"copyAndRemoveFile", copyAndRemoveFile, dstExists},
		{"copyAndRemoveFile", copyAndRemoveFile, dstIsSrc},
	} {
		t.Run(fmt.Sprintf("%v-%v", tc.name, tc.errorState), func(t *testing.T) {
			const data = "here's the data to copy\n"
			const existing = "here's existing data\n"

			td := t.TempDir()
			srcPath := filepath.Join(td, "src", "file.txt")
			if err := os.Mkdir(filepath.Dir(srcPath), 0755); err != nil {
				t.Fatal(err)
			}
			if err := os.WriteFile(srcPath, []byte(data), 0644); err != nil {
				t.Fatal(err)
			}

			dstPath := filepath.Join(td, "dst", "out.txt")
			switch tc.errorState {
			case dstExists:
				if err := os.Mkdir(filepath.Dir(dstPath), 0755); err != nil {
					t.Fatal(err)
				}
				if err := os.WriteFile(dstPath, []byte(existing), 0644); err != nil {
					t.Fatal(err)
				}
			case dstIsSrc:
				dstPath = srcPath
			case dstIsSrcWithDot:
				dstPath = filepath.Join(filepath.Dir(srcPath), ".", filepath.Base(srcPath))
			}

			if err := tc.fn(srcPath, dstPath); tc.errorState == noError {
				if err != nil {
					t.Fatalf("%v(%q, %q) failed: %v", tc.name, srcPath, dstPath, err)
				}
				if b, err := os.ReadFile(dstPath); err != nil {
					t.Error(err)
				} else if string(b) != data {
					t.Errorf("%v wrote %q; want %q", tc.name, string(b), data)
				}
				if _, err := os.Stat(srcPath); err == nil {
					t.Errorf("%v still exists", srcPath)
				}
			} else {
				if err == nil {
					t.Errorf("%v(%q, %q) unexpectedly succeeded", tc.name, srcPath, dstPath)
				}
				if _, err := os.Stat(srcPath); err != nil {
					t.Error(err)
				}
			}
		})
	}
}

// writeTestFileInfos writes the files described by infos under dir using makeTestData.
func writeTestFileInfos(t *testing.T, dir string, infos []*fileInfo) {
	for _, fi := range infos {
		p := filepath.Join(dir, fi.path)
		if err := os.MkdirAll(filepath.Dir(p), 0755); err != nil {
			t.Fatal(err)
		}
		if err := os.WriteFile(p, []byte(makeTestData(fi.size)), 0644); err != nil {
			t.Fatal(err)
		}
	}
}

// makeTestData repeats 'a' size times.
func makeTestData(size int64) string { return strings.Repeat("a", int(size)) }

// checkTestDir reads all the files under dir and verifies that they match want,
// a map from relative path to data.
func checkTestDir(t *testing.T, dir string, want map[string]string) {
	t.Helper()
	type file struct{ path, data string }

	var wantFiles []file
	for p, d := range want {
		wantFiles = append(wantFiles, file{p, d})
	}
	sort.Slice(wantFiles, func(i, j int) bool { return wantFiles[i].path < wantFiles[j].path })

	var gotFiles []file
	if err := filepath.Walk(dir, func(p string, fi os.FileInfo, err error) error {
		if p == dir {
			return nil // ignore root dir not existing
		} else if err != nil {
			return err
		} else if !fi.Mode().IsRegular() {
			return nil
		}
		b, err := os.ReadFile(p)
		if err != nil {
			return err
		}
		gotFiles = append(gotFiles, file{p[len(dir)+1:], string(b)})
		return nil
	}); err != nil {
		t.Error(err)
		return
	}

	if !reflect.DeepEqual(gotFiles, wantFiles) {
		dump := func(files []file) string {
			lines := make([]string, len(files))
			for i, f := range files {
				lines[i] = fmt.Sprintf("  %v: %q", f.path, f.data)
			}
			return strings.Join(lines, "\n")
		}
		t.Errorf("%v doesn't contain expected files\nGot:\n%v\nWant:\n%v\n",
			dir, dump(gotFiles), dump(wantFiles))
	}
}
